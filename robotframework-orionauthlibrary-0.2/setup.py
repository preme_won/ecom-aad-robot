from setuptools import setup
from os.path import abspath, dirname, join

LIBRARY_NAME = 'OrionAuthLibrary'
CWD = abspath(dirname(__file__))
execfile(join(CWD, 'src', LIBRARY_NAME, 'version.py'))

CLASSIFIERS = """
Development Status :: 5 - Production/Stable
License :: Public Domain
Operating System :: OS Independent
Programming Language :: Python
Topic :: Software Development :: Testing
"""[1:-1]

setup(name         = 'robotframework-orionauthlibrary',
      version      = VERSION,
      description  = 'Robot Framework keyword library for Wemall\'s Authentication, Authorization service',
      author       = 'Traitanit Huangsri',
      author_email = 'traitanit.hua@ascendinter.com',
      url          = 'http://git.itruemart-dev.com/tmn/robot-lib.git',
      license      = 'Public Domain',
      platforms    = 'any',
      classifiers  = CLASSIFIERS.splitlines(),
      package_dir  = {'' : 'src'},
      packages     = [LIBRARY_NAME],
      package_data = {LIBRARY_NAME: ['util/*']},
      install_requires = ['robotframework', 'pyjwt', 'cryptography']
)