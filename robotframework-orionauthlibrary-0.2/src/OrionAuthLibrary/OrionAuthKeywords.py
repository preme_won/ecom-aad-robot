__author__ = 'Traitanit H.'
from robot.api import logger

class OrionAuthKeywords(object):

    string_util = None

    def __init__(self):
        pass

    def delete_wemall_profile_from_uid_trueid(self, db, uid_of_trueid):
        """ Delete Wemall user's profile data on database

        Arguments:
            - uid_of_trueid: UID of TrueID(After login via TrueID Complete will get UID)

        Examples:
        | Delete Wemall Profile From Uid Trueid | 1114567890  |
        """
        cursor = db.cursor()
        try:
            if db is None:
                logger.error("You're not connecting to the database, please call \'connect\' first")
                raise Exception
            cursor.execute("SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED")
            sql = "select ui.user_id from user_identity ui where ui.identity_id = %(uid)s order by ui.user_id desc"
            cursor.execute(sql, {'uid': uid_of_trueid})
            user_id = cursor.fetchone()

            if not user_id:
                logger.info("Cannot find Wemall User's profile from UID : "+str(uid_of_trueid))
                return
            logger.info("Delete Wemall user-id : "+str(user_id[0]))
            cursor.execute("delete from user where id = %(user_id)s", {'user_id': user_id[0]})
            cursor.execute("delete from user_identity where user_id = %(user_id)s", {'user_id': user_id[0]})
            cursor.execute("delete from valid_refresh_token where user_id = %(user_id)s", {'user_id': user_id[0]})
            db.commit()

        except Exception as ex:
            logger.error(str(ex))
            raise Exception

    def _get_user_id(self, db, prefix):
        cursor = db.cursor()
        try:
            if db is None:
                logger.error("You're not connecting to the database, please call \'connect\' first")
                raise Exception
            cursor.execute("SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED")
            # shopper&merchant user_id
            prefix = prefix + "%"
            sql = "select s.user_id from shopper s where s.display_name like %(prefix)s union " \
                  "select m.user_id from user_password m where m.username like %(prefix)s"
            cursor.execute(sql, {'prefix': "%"+prefix+"%"})
            rows = cursor.fetchall()
            result = []
            for row in rows:
                result.append(row[0])
            return result
        except Exception as ex:
            logger.error(str(ex))
            raise Exception

    def _get_role_id(self, db, prefix):
        cursor = db.cursor()
        try:
            if db is None:
                logger.error("You're not connecting to the database, please call \'connect\' first")
                raise Exception
            cursor.execute("SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED")
            # role id
            sql = "select r.id from role r where r.name like %(role_prefix)s" 
            cursor.execute(sql, {'role_prefix': prefix + "%"})
            rows = cursor.fetchall()
            result = []
            for row in rows:
                result.append(row[0])
            return result
        except Exception as ex:
            logger.error(str(ex))
            raise Exception

    def _get_feature_id(self, db, prefix):
        cursor = db.cursor()
        try:
            if db is None:
                logger.error("You're not connecting to the database, please call \'connect\' first")
                raise Exception
            cursor.execute("SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED")
            # feature id
            sql = "select f.id from feature f where f.name like %(feature_prefix)s" 
            cursor.execute(sql, {'feature_prefix': prefix + "%"})
            rows = cursor.fetchall()
            result = []
            for row in rows:
                result.append(row[0])
            return result
        except Exception as ex:
            logger.error(str(ex))
            raise Exception

    def delete_wemall_user_from_prefix(self, db, prefix):
        """ Delete Wemall user's profile data from prefix email

        Arguments:
            - prefix: prefix of wemall/TrueId email

        Examples:
        | Delete Wemall User From Prefix | robot_aad  |
        """
        cursor = db.cursor()
        try:
            user_id_list = self._get_user_id(db, prefix)
            if len(user_id_list) == 0:
                logger.info("Cannot found user_id from prefix: "+prefix)
                return

            for user_id in user_id_list:
                logger.info("deleting user_id: "+str(user_id))
                cursor.execute('delete from user_identity where user_id = %(user_id)s', {'user_id': user_id})
                cursor.execute('delete from user where id = %(user_id)s', {'user_id': user_id})
                cursor.execute('delete from merchant where user_id = %(user_id)s', {'user_id': user_id})
                cursor.execute('delete from shopper where user_id = %(user_id)s', {'user_id': user_id})
                cursor.execute('delete from user_password where user_id = %(user_id)s', {'user_id': user_id})
                cursor.execute('delete from valid_refresh_token where user_id = %(user_id)s', {'user_id': user_id})
                db.commit()
        except Exception as ex:
            logger.error(str(ex))
            raise Exception

    def clear_authorization_data_from_prefix(self, db, prefix):
        """ Delete authorization data from prefix of role/feature

        Arguments:
            - prefix: prefix of role/feature

        Examples:
        | Clear Authorization Data From Prefix | robot_ |
        """
        cursor = db.cursor()
        try:
            role_id_list = self._get_role_id(db, prefix)

            if len(role_id_list) == 0:
                logger.info("Cannot found authorization data from role's prefix: "+prefix)

            # /* role_feature_permission */
            for role_id in role_id_list:
                logger.info("deleting role_feature_permission from role_id: "+str(role_id))
                cursor.execute('delete from role_feature_permission where role_id = %(role_id)s', {'role_id': role_id})
                db.commit()

            feature_id_list = self._get_feature_id(db, prefix)
            if len(feature_id_list) == 0:
                logger.info("Cannot found authorization data from feature's prefix: "+prefix)

            # /* role */
            for role_id in role_id_list:
                logger.info("deleting role_id: "+str(role_id))
                cursor.execute('delete from role where id = %(role_id)s', {'role_id': role_id})
                db.commit()

            # /* feature */
            for feature_id in feature_id_list:
                logger.info("deleting feature_id: "+str(feature_id))
                cursor.execute('delete from feature where id = %(feature_id)s', {'feature_id': feature_id})
                db.commit()

            for feature_id in feature_id_list:
                logger.info("deleting role_feature_permission from feature_id: "+str(feature_id))
                cursor.execute('delete from role_feature_permission where feature_id = %(feature_id)s', {'feature_id': feature_id})
                db.commit()

            # /* user_shop_role */
            for role_id in role_id_list:
                logger.info("deleting user_shop_role from role_id: "+str(role_id))
                cursor.execute('delete from user_shop_role where role_id = %(role_id)s', {'role_id': role_id})
                db.commit()

        except Exception as ex:
            logger.error(str(ex))
            raise Exception

    def clear_authorization_mapping_data_from_prefix(self, db, db_authz, prefix):
        """ Delete authorization mapping data from prefix of user_id

        Arguments:
            - db: Database connection object for authentication service
            - db_authz: Database connection object for authorization service
            - prefix: prefix of username

        Examples:
        | Clear Authorization Mapping Data From Prefix | ${db}  | ${db_authz} | robot_
        """
        authz_cursor = db_authz.cursor()
        try:
            user_id_list = self._get_user_id(db, prefix)
            if len(user_id_list) == 0:
                logger.info("Cannot found user_id from username with prefix: "+prefix)
                return

            # /* user_shop_role */
            for user_id in user_id_list:
                logger.info("deleting user_shop_role from user_id: "+str(user_id))
                authz_cursor.execute('delete from user_shop_role where user_id = %(user_id)s', {'user_id': user_id})
                db_authz.commit()

        except Exception as ex:
            logger.error(str(ex))
            raise Exception

    def clear_shop_data_from_shopId(self, db, shopId_list):
        """ Delete shop data from shopId_list

        Arguments:
            - shopId_list: List of shopId

        Examples:
        | Clear Shop Data From shopId_list | ('MTH10023','MTH10024') |
        """
        cursor = db.cursor()
        try:
            # /* shop */
            for shopId in shopId_list:
                logger.info("deleting shop from shopId : "+str(shopId))
                cursor.execute('delete from shop where id = %(shopId)s', {'shopId': shopId})
                db.commit()        

        except Exception as ex:
            logger.error(str(ex))
            raise Exception

    def count_all_unique_merchant_user(self, db):


        try:
            auth_cursor = db.cursor()
            auth_cursor.execute("SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED")
            auth_cursor.execute("SELECT u.id FROM user u LEFT JOIN merchant m ON u.id = m.user_id LEFT JOIN user_password pwd ON u.id = pwd.user_id WHERE u.type = 'MERCHANT'")
            rows = auth_cursor.fetchall()
            user_id_list = set()
            for row in rows:
                user_id_list.add(row[0])
            logger.info("Unique user_id="+str(user_id_list))
            return len(user_id_list)
        except Exception as ex:
            logger.error(str(ex))
            raise Exception

    def count_users_from_user_shop_role_by_role_id(self, db, db_authz, role_id):
        try:
            if db is None or db_authz is None:
                logger.error("You're not connecting to the database, please call \'connect\' first")
                raise Exception

            auth_cursor = db.cursor(buffered=True)
            authz_cursor = db_authz.cursor(buffered=True)
            auth_cursor.execute("SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED")
            authz_cursor.execute("SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED")
            users_set = set()
            sql = 'select distinct(user_id) from user_shop_role where role_id = %(role_id)s'
            authz_cursor.execute(sql, {'role_id': role_id})
            rows = authz_cursor.fetchall()
            for row in rows:
                users_set.add(row[0])
            logger.debug("user_id set: "+str(users_set))

            result_set = set()
            for user_id in users_set:
                sql = 'select user_id from merchant where user_id = %(user_id)s'
                auth_cursor.execute(sql, {'user_id': user_id})
                row = auth_cursor.fetchone()
                if row is not None:
                    result_set.add(row[0])

            return len(result_set)
        except Exception as ex:
            logger.error(str(ex))
            raise Exception


