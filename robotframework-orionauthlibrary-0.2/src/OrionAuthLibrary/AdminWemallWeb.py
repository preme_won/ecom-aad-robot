from robot.libraries.BuiltIn import BuiltIn
from robot.api import logger

class AdminWemallWeb(object):

    def find_user_list_on_table(self, user_id, shopName):
        """ Find user list on table after search

        Arguments:
            - user_id: user id want find
            - shopName: shop name want find

        Examples:
        | Find User List    | 40  |  iTrueMark
        """
        selenium = BuiltIn().get_library_instance('ExtendedSelenium2Library')

        xpathTable = '//table[@id="userDatatable"]/tbody'
        numRowUser = selenium.get_matching_xpath_count(xpathTable)

        for row in range(1, int(numRowUser)+1):
            xpath = xpathTable + '['+str(row)+']/tr'
            numRowEachUser = selenium.get_matching_xpath_count(xpath)

            for rowEachUser in range(1, int(numRowEachUser)+1):

                user_id_xpath = xpath+'['+str(rowEachUser)+']/td[1]'
                shop_xpath = xpath+'['+str(rowEachUser)+']/td[6]'

                user_id_from_table = selenium.get_text('xpath='+user_id_xpath)
                shop_from_table = selenium.get_text('xpath='+shop_xpath)
                logger.info("compare with user id: "+str(user_id_from_table)+", shop "+str(shop_from_table)+" from table result user list")
                if user_id_from_table.strip() == str(user_id).strip() and shop_from_table.strip() == shopName.strip():
                    result = {}
                    username_xpath = xpath+'['+str(rowEachUser)+']/td[2]'
                    firstName_xpath = xpath+'['+str(rowEachUser)+']/td[3]'
                    lastName_xpath = xpath+'['+str(rowEachUser)+']/td[4]'
                    email_xpath = xpath+'['+str(rowEachUser)+']/td[5]'
                    role_xpath = xpath+'['+str(rowEachUser)+']/td[7]'

                    result['id'] = selenium.get_text('xpath='+user_id_xpath)
                    result['username'] = selenium.get_text('xpath='+username_xpath)
                    result['firstName'] = selenium.get_text('xpath='+firstName_xpath)
                    result['lastName'] = selenium.get_text('xpath='+lastName_xpath)
                    result['email'] = selenium.get_text('xpath='+email_xpath)
                    result['shop'] = selenium.get_text('xpath='+shop_xpath)
                    result['role'] = selenium.get_text('xpath='+role_xpath)
                    return result

        logger.info("Can't find user id: "+str(user_id)+" and shop "+str(shopName)+" from table result user list")
        raise Exception