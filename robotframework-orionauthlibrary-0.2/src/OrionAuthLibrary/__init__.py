from version import VERSION
from OrionAuthKeywords import OrionAuthKeywords
from util.Base64Util import Base64Util
from util.StringUtil import StringUtil
from util.URLUtil import URLUtil
from AdminWemallWeb import AdminWemallWeb
from util.DictionaryUtil import DictionaryUtil
_version_ = VERSION


class OrionAuthLibrary(OrionAuthKeywords, StringUtil, Base64Util, URLUtil, DictionaryUtil, AdminWemallWeb):

    ROBOT_LIBRARY_SCOPE = 'TEST CASE'
