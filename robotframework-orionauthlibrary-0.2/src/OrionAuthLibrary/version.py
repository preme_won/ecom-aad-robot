VERSION = '0.2'


def get_version():
    """Returns the current version."""
    return VERSION
