__author__ = 'Traitanit'
from robot.api import logger

class DictionaryUtil(object):

    def should_be_equal_as_dictionaries(self, dict1, dict2):
        """ Compare two dictionaries (json) Return true if dict2 matches with dict1

        Arguments:
            - dict1
            - dict2

        Examples:
        | Should Be Equal As Dictionaries | ${dict1}  |  ${dict2} |
        """
        cmp_result = cmp(dict1, dict2)
        if cmp_result is not 0:
            logger.error("2 dictionaries does not match")
            raise Exception
        else:
            return


if __name__ == "__main__":
    dict1 = {'Name': 'Zara', 'Age': 7};
    dict2 = {'Name': 'Mahnaz', 'Age': 27};
    dict3 = {'Name': 'Abid', 'Age': 27};
    dict4 = {'Name': 'Zara', 'Age': 9};
    dict_util = DictionaryUtil()
    dict_util.should_be_equal_as_dictionaries(dict1, dict4)
