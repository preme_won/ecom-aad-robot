import urllib


class URLUtil(object):

    def decode_url(self, url):
        return urllib.unquote(url).decode('utf8')

    def encode_url(self, url):
        return urllib.quote(url, safe='').encode('utf8')
