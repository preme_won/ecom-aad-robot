__author__ = 'Traitanit H.'
import string
import re


class StringUtil(object):

    def convert_number_to_alphabet(self, input_string):
        """ Convert Number To English Alphabet
        1=a, 2=b, 3=c, ...

        Arguments:
            - input_string

        Examples:
        | ${replaced}=               | Convert Number To Alphabet | robot_123456 |
        | Should Be Equal As Strings | robot_abcdef               | ${replaced}  |
        """
        output = []
        num2alpha = dict(zip(range(0, 26), string.ascii_lowercase))
        for char in input_string:
            try:
                number = int(char)
                output.append(num2alpha[int(number)])
            except ValueError:
                output.append(char)

        return ''.join(output)

    def remove_sql_comments(self, file_path):
        """ Remove comments out of SQL statements
        Comment pattern like /*  */

        Arguments:
            - SQL File Path

        Examples:
        | ${sql_statement}=          | Remove Sql Comments | /opt/mysql.sql |
        """
        file = open(file_path, 'r')
        output = ''
        for line in file:
            result = re.match('/\*(.*)\*/', line)
            if result is not None:
                continue
            else:
                output += line.rstrip()
        return output