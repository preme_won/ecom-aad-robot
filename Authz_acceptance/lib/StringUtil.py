__author__ = 'Traitanit H.'
import string
import re

def convert_number_to_alphabet(input_string):
    output = []
    num2alpha = dict(zip(range(0, 26), string.ascii_lowercase))
    for char in input_string:
        try:
            number = int(char)
            output.append(num2alpha[int(number)])
        except ValueError:
            output.append(char)

    return ''.join(output)

def remove_sql_comments(file_path):
    file = open(file_path, 'r')
    output = ''
    for line in file:
        result = re.match('/\*(.*)\*/', line)
        if result is not None:
            continue
        else:
            output += line.rstrip()
    return output

if __name__ == "__main__":
    '''
    str = 'asdasdadsadasadqkwer.eqw'
    print "input="+str
    str = convert_number_to_alphabet(str)
    print "output="+str
    '''
    remove_sql_comments('D:/test/sql.txt')


