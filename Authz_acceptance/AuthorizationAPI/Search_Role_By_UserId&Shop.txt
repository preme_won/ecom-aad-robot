*** Settings ***
Suite Setup       Search Role Suite Setup
Test Teardown     Revoke Token and Delete Session    ${refresh_token}
Force Tags        ready    regression    authorization-api    getRoleByShopAndUserId
Resource          ../resources/keyword_resource.txt

*** Test Cases ***
TC_WeMall_00159
    [Documentation]    Admin search role for own user within the shop and user was assigned in 1 role
    # Admin Login
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${user_nike_admin}    ${password}    grant_type=merchant
    Validate Login Completed    ${response}
    ${access_token}=    Get Access Token from Response    ${response.text}
    ${refresh_token}=    Get Refresh Token from Response    ${response.text}
    ${header}=    Create Dictionary    ${header_token_authz}=${access_token}
    # Search Merchant To Get user_id first
    ${response}=    Search Merchant    ${user_nike_admin}    ${header}
    Response Should Be 200 OK    ${response}
    ${merchant}=    Find Merchant Username In Response    ${response}    ${user_nike_admin}
    # Search Role from user_id and shop_id
    ${response}=    Get Role by Shop and UserId(API)    ${merchant['id']}    ${shopName_Nike}    ${header}
    Response Should Be 200 OK    ${response}
    ${expect_role_id}=    Get RoleID by Description    ${db_authz}    ${role_admin}
    Verify Number of Roles in response    ${response}    1
    ${role_response}=    Find role in response    ${response}    ${expect_role_id}
    Verify Search Role Response    ${role_response}    ${expect_role_id}    ${role_admin}

TC_WeMall_00160
    [Documentation]    Admin search role for another user within the shop and user was assigned only 1 role
    # Login with ${user_nike_admin}
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${user_nike_admin}    ${password}    grant_type=merchant
    Validate Login Completed    ${response}
    ${access_token}=    Get Access Token from Response    ${response.text}
    ${refresh_token}=    Get Refresh Token from Response    ${response.text}
    ${header}=    Create Dictionary    ${header_token_authz}=${access_token}
    # Search Merchant To Get user_id first
    ${response}=    Search Merchant    ${user_nike_mkt}    ${header}
    Response Should Be 200 OK    ${response}
    ${merchant}=    Find Merchant Username In Response    ${response}    ${user_nike_mkt}
    # Get Role by user_id and shop_id
    ${response}=    Get Role by Shop and UserId(API)    ${merchant['id']}    ${shopName_Nike}    ${header}
    Response Should Be 200 OK    ${response}
    ${expect_role_id}=    Get RoleID by Description    ${db_authz}    ${role_marketing}
    Verify Number of Roles in response    ${response}    1
    ${role_response}=    Find role in response    ${response}    ${expect_role_id}
    Verify Search Role Response    ${role_response}    ${expect_role_id}    ${role_marketing}

TC_WeMall_00161
    [Documentation]    Admin search role for another user within the shop and user was assigned more than 1 role
    # Admin Login
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${user_nike_admin}    ${password}    grant_type=merchant
    Validate Login Completed    ${response}
    ${access_token}=    Get Access Token from Response    ${response.text}
    ${refresh_token}=    Get Refresh Token from Response    ${response.text}
    ${header}=    Create Dictionary    ${header_token_authz}=${access_token}
    # Search Merchant To Get user_id first
    ${response}=    Search Merchant    ${user_nike_prod_mkt}    ${header}
    Response Should Be 200 OK    ${response}
    ${merchant}=    Find Merchant Username In Response    ${response}    ${user_nike_prod_mkt}
    # Search Role from user_id and shop_id
    ${response}=    Get Role by Shop and UserId(API)    ${merchant['id']}    ${shopName_Nike}    ${header}
    Response Should Be 200 OK    ${response}
    ${role_marketing_id}=    Get RoleID by Description    ${db_authz}    ${role_marketing}
    ${role_product_id}=    Get RoleID by Description    ${db_authz}    ${role_product}
    Verify Number of Roles in response    ${response}    2
    # Verify Marketing Role
    ${role_response}=    Find role in response    ${response}    ${role_marketing_id}
    Verify Search Role Response    ${role_response}    ${role_marketing_id}    ${role_marketing}
    # Verify Product Role
    ${role_response}=    Find role in response    ${response}    ${role_product_id}
    Verify Search Role Response    ${role_response}    ${role_product_id}    ${role_product}

TC_WeMall_00162
    [Documentation]    Admin search role for another user within the shop and user was assigned to all role
    # Admin Login
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${user_nike_admin}    ${password}    grant_type=merchant
    Validate Login Completed    ${response}
    ${access_token}=    Get Access Token from Response    ${response.text}
    ${refresh_token}=    Get Refresh Token from Response    ${response.text}
    ${header}=    Create Dictionary    ${header_token_authz}=${access_token}
    # Search Merchant To Get user_id first
    ${response}=    Search Merchant    ${user_nike_all}    ${header}
    Response Should Be 200 OK    ${response}
    ${merchant}=    Find Merchant Username In Response    ${response}    ${user_nike_all}
    # Search Role from user_id and shop_id
    ${response}=    Get Role by Shop and UserId(API)    ${merchant['id']}    ${shopName_Nike}    ${header}
    Response Should Be 200 OK    ${response}
    ${role_all_id}=    Get RoleID by Description    ${db_authz}    ${role_all}
    Verify Number of Roles in response    ${response}    1
    # Verify Marketing Role
    ${role_response}=    Find role in response    ${response}    ${role_all_id}
    Verify Search Role Response    ${role_response}    ${role_all_id}    ${role_all}

TC_WeMall_00163
    [Documentation]    Admin search role for another user across the shop but he doesn't has permission on that shop
    [Tags]
    # Admin Login
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${user_nike_admin}    ${password}    grant_type=merchant
    Validate Login Completed    ${response}
    ${access_token}=    Get Access Token from Response    ${response.text}
    ${refresh_token}=    Get Refresh Token from Response    ${response.text}
    ${header}=    Create Dictionary    ${header_token_authz}=${access_token}
    # Search Merchant To Get user_id first
    ${response}=    Search Merchant    ${user_nb_mkt}    ${header}
    Response Should Be 200 OK    ${response}
    ${merchant}=    Find Merchant Username In Response    ${response}    ${user_nb_mkt}
    # Search Role from user_id and shop_id
    ${response}=    Get Role by Shop and UserId(API)    ${merchant['id']}    ${shopName_Nb}    ${header}
    Response Should Be 403 Forbidden    ${response}
    Verify ErrorCode and ErrorMsg From Response    ${response.text}    ${errorCode_permission_not_enough}    ${errorMessage_authz_require_manage_user}

TC_WeMall_00164
    [Documentation]    Admin with noperm user search role for another user within the shop
    [Tags]
    # Admin Login
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${user_nike_admin_noperm}    ${password}    grant_type=merchant
    Validate Login Completed    ${response}
    ${access_token}=    Get Access Token from Response    ${response.text}
    ${refresh_token}=    Get Refresh Token from Response    ${response.text}
    ${header}=    Create Dictionary    ${header_token_authz}=${access_token}
    # Search Merchant To Get user_id first
    ${response}=    Search Merchant    ${user_nike_mkt}    ${header}
    Response Should Be 200 OK    ${response}
    ${merchant}=    Find Merchant Username In Response    ${response}    ${user_nike_mkt}
    # Search Role from user_id and shop_id
    ${response}=    Get Role by Shop and UserId(API)    ${merchant['id']}    ${shopName_Nike}    ${header}
    Response Should Be 403 Forbidden    ${response}
    Verify ErrorCode and ErrorMsg From Response    ${response.text}    ${errorCode_permission_not_enough}    ${errorMessage_authz_require_manage_user}

TC_WeMall_00165
    [Documentation]    User which has no feature 'manage_user' cannot search role from any user nor shop
    [Tags]
    # Admin Login
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${user_nike_mkt}    ${password}    grant_type=merchant
    Validate Login Completed    ${response}
    ${access_token}=    Get Access Token from Response    ${response.text}
    ${refresh_token}=    Get Refresh Token from Response    ${response.text}
    ${header}=    Create Dictionary    ${header_token_authz}=${access_token}
    # Search Merchant To Get user_id first
    ${response}=    Search Merchant    ${user_nike_prod_mkt}    ${header}
    Response Should Be 200 OK    ${response}
    ${merchant}=    Find Merchant Username In Response    ${response}    ${user_nike_prod_mkt}
    # Search Role from user_id and shop_id
    ${response}=    Get Role by Shop and UserId(API)    ${merchant['id']}    ${shopName_Nike}    ${header}
    Response Should Be 403 Forbidden    ${response}
    Verify ErrorCode and ErrorMsg From Response    ${response.text}    ${errorCode_permission_not_enough}    ${errorMessage_authz_require_manage_user}

TC_WeMall_00166
    [Documentation]    Admin who sit under multiple shop can search for role across shop and user
    [Tags]
    # Admin Login
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${user_nike_nb_admin}    ${password}    grant_type=merchant
    Validate Login Completed    ${response}
    ${access_token}=    Get Access Token from Response    ${response.text}
    ${refresh_token}=    Get Refresh Token from Response    ${response.text}
    ${header}=    Create Dictionary    ${header_token_authz}=${access_token}
    # Search Merchant To Get user_id first
    ${response}=    Search Merchant    ${user_nb_mkt}    ${header}
    Response Should Be 200 OK    ${response}
    ${merchant}=    Find Merchant Username In Response    ${response}    ${user_nb_mkt}
    # Search Role from user_id and shop_id
    ${response}=    Get Role by Shop and UserId(API)    ${merchant['id']}    ${shopName_Nb}    ${header}
    Response Should Be 200 OK    ${response}
    ${role_marketing_id}=    Get RoleID by Description    ${db_authz}    ${role_marketing}
    Verify Number of Roles in response    ${response}    1
    # Verify Marketing Role
    ${role_response}=    Find role in response    ${response}    ${role_marketing_id}
    Verify Search Role Response    ${role_response}    ${role_marketing_id}    ${role_marketing}

TC_WeMall_00167
    [Documentation]    Admin who sit on shop 'all' can search role for user who sit under shop 'all' as well
    [Tags]
    # Admin Login
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${user_allshop_allrole}    ${password}    grant_type=merchant
    Validate Login Completed    ${response}
    ${access_token}=    Get Access Token from Response    ${response.text}
    ${refresh_token}=    Get Refresh Token from Response    ${response.text}
    ${header}=    Create Dictionary    ${header_token_authz}=${access_token}
    # Search Merchant To Get user_id first
    ${response}=    Search Merchant    ${user_mkt_all}    ${header}
    Response Should Be 200 OK    ${response}
    ${merchant}=    Find Merchant Username In Response    ${response}    ${user_mkt_all}
    # Search Role from user_id and shop_id
    ${response}=    Get Role by Shop and UserId(API)    ${merchant['id']}    ${shopId_all}    ${header}
    Response Should Be 200 OK    ${response}
    ${role_marketing_id}=    Get RoleID by Description    ${db_authz}    ${role_marketing}
    Verify Number of Roles in response    ${response}    1
    # Verify Marketing Role
    ${role_response}=    Find role in response    ${response}    ${role_marketing_id}
    Verify Search Role Response    ${role_response}    ${role_marketing_id}    ${role_marketing}

TC_WeMall_00168
    [Documentation]    Admin search role with invalid token
    [Tags]
    # Admin Login
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${user_nike_admin}    ${password}    grant_type=merchant
    Validate Login Completed    ${response}
    ${access_token}=    Get Access Token from Response    ${response.text}
    ${refresh_token}=    Get Refresh Token from Response    ${response.text}
    ${header}=    Create Dictionary    ${header_token_authz}=${access_token}
    ${header_invalid}=    Create Dictionary    ${header_token_authz}=${access_token}_invalid
    # Search Merchant To Get user_id first
    ${response}=    Search Merchant    ${user_nike_mkt}    ${header}
    Response Should Be 200 OK    ${response}
    ${merchant}=    Find Merchant Username In Response    ${response}    ${user_nike_mkt}
    # Search Role from user_id and shop_id
    ${response}=    Get Role by Shop and UserId(API)    ${merchant['id']}    ${shopName_Nike}    ${header_invalid}
    Response Should Be 401 Unauthorized    ${response}
    Verify ErrorCode and ErrorMsg From Response    ${response.text}    ${errorCode_unauthorized_request}    ${errorMessage_authz_with_invalid_token}

TC_WeMall_00169
    [Documentation]    Admin search role without token
    [Tags]
    # Admin Login
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${user_nike_admin}    ${password}    grant_type=merchant
    Validate Login Completed    ${response}
    ${access_token}=    Get Access Token from Response    ${response.text}
    ${refresh_token}=    Get Refresh Token from Response    ${response.text}
    ${header}=    Create Dictionary    ${header_token_authz}=${access_token}
    # Search Merchant To Get user_id first
    ${response}=    Search Merchant    ${user_nike_mkt}    ${header}
    Response Should Be 200 OK    ${response}
    ${merchant}=    Find Merchant Username In Response    ${response}    ${user_nike_mkt}
    # Search Role from user_id and shop_id
    ${response}=    Get Role by Shop and UserId(API)    ${merchant['id']}    ${shopName_Nike}    ${EMPTY}
    Response Should Be 401 Unauthorized    ${response}
    Verify ErrorCode and ErrorMsg From Response    ${response.text}    ${errorCode_unauthorized_request}    ${errorMessage_authz_without_token}

TC_WeMall_00170
    [Documentation]    Admin search role with invalid userId
    [Tags]
    # Admin Login
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${user_nike_admin}    ${password}    grant_type=merchant
    Validate Login Completed    ${response}
    ${access_token}=    Get Access Token from Response    ${response.text}
    ${refresh_token}=    Get Refresh Token from Response    ${response.text}
    ${header}=    Create Dictionary    ${header_token_authz}=${access_token}
    # Search Role from user_id and shop_id
    ${response}=    Get Role by Shop and UserId(API)    0    ${shopName_Nike}    ${header}
    Response Should Be 200 OK    ${response}
    Verify Number of Roles in response    ${response}    0
    ${response}=    Get Role by Shop and UserId(API)    10002_user    ${shopName_Nike}    ${header}
    Response Should Be 400 Bad Request    ${response}
    Verify ErrorCode and ErrorMsg From Response    ${response.text}    ${errorCode_invalid_param}    ${errorMessage_userId_must_be_number}

TC_WeMall_00171
    [Documentation]    Admin search role with non-existing shop
    [Tags]
    # Admin Login
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${user_nike_admin}    ${password}    grant_type=merchant
    Validate Login Completed    ${response}
    ${access_token}=    Get Access Token from Response    ${response.text}
    ${refresh_token}=    Get Refresh Token from Response    ${response.text}
    ${header}=    Create Dictionary    ${header_token_authz}=${access_token}
    # Search Merchant To Get user_id first
    ${response}=    Search Merchant    ${user_nike_mkt}    ${header}
    Response Should Be 200 OK    ${response}
    ${merchant}=    Find Merchant Username In Response    ${response}    ${user_nike_mkt}
    # Search Role from user_id and shop_id
    ${response}=    Get Role by Shop and UserId(API)    ${merchant['id']}    robot_invalid_shop    ${header}
    Response Should Be 403 Forbidden    ${response}
    Verify ErrorCode and ErrorMsg From Response    ${response.text}    ${errorCode_permission_not_enough}    ${errorMessage_authz_require_manage_user}

TC_WeMall_00172
    [Documentation]    Admin which has 'manage_user' permission for some shop should get role only for that shop
    [Tags]
    # Admin Login
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${user_nike_noperm_nb_perm}    ${password}    grant_type=merchant
    Validate Login Completed    ${response}
    ${access_token}=    Get Access Token from Response    ${response.text}
    ${refresh_token}=    Get Refresh Token from Response    ${response.text}
    ${header}=    Create Dictionary    ${header_token_authz}=${access_token}
    # Search Merchant To Get user_id first
    ${response}=    Search Merchant    ${user_nike_noperm_nb_perm}    ${header}
    Response Should Be 200 OK    ${response}
    ${merchant}=    Find Merchant Username In Response    ${response}    ${user_nike_noperm_nb_perm}
    ${user_nike_mkt_id}=    Set Variable    ${merchant['id']}
    # Search Role from user_id and shop_id
    ${response}=    Get Role by Shop and UserId(API)    ${user_nike_mkt_id}    ${shopName_Nike}    ${header}
    Response Should Be 403 Forbidden    ${response}
    Verify ErrorCode and ErrorMsg From Response    ${response.text}    ${errorCode_permission_not_enough}    ${errorMessage_authz_require_manage_user}
    ${response}=    Get Role by Shop and UserId(API)    ${user_nike_mkt_id}    ${shopName_Nb}    ${header}
    Response Should Be 200 OK    ${response}
    ${expect_role_id}=    Get RoleID by Description    ${db_authz}    ${role_admin}
    Verify Number of Roles in response    ${response}    1
    ${role_response}=    Find role in response    ${response}    ${expect_role_id}
    Verify Search Role Response    ${role_response}    ${expect_role_id}    ${role_admin}

TC_WeMall_00175
    [Documentation]    Admin who sit on shop 'all' can search role for user who sit under another shop
    [Tags]
    # Admin Login
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${user_allshop_allrole}    ${password}    grant_type=merchant
    Validate Login Completed    ${response}
    ${access_token}=    Get Access Token from Response    ${response.text}
    ${refresh_token}=    Get Refresh Token from Response    ${response.text}
    ${header}=    Create Dictionary    ${header_token_authz}=${access_token}
    # Search Merchant To Get user_id first
    ${response}=    Search Merchant    ${user_nike_mkt}    ${header}
    Response Should Be 200 OK    ${response}
    ${merchant}=    Find Merchant Username In Response    ${response}    ${user_nike_mkt}
    ${user_nike_mkt_id}=    Set Variable    ${merchant['id']}
    # Search Role from user_id and shop_id
    ${response}=    Get Role by Shop and UserId(API)    ${user_nike_mkt_id}    ${shopName_Nike}    ${header}
    Response Should Be 200 OK    ${response}
    ${expect_role_id}=    Get RoleID by Description    ${db_authz}    ${role_marketing}
    Verify Number of Roles in response    ${response}    1
    ${role_response}=    Find role in response    ${response}    ${expect_role_id}
    Verify Search Role Response    ${role_response}    ${expect_role_id}    ${role_marketing}

*** Keywords ***
Search Role Suite Setup
    ${db}=    Connect    ${database_name}    ${database_username}    ${database_password}    ${database_host}    ${database_port}
    Set Suite Variable    ${db}    ${db}
    ${db_authz}=    Connect    ${database_authz_name}    ${database_authz_username}    ${database_authz_password}    ${database_authz_host}    ${database_port}
    Set Suite Variable    ${db_authz}    ${db_authz}
    Clear Authorization Mapping Data From Prefix    ${db}    ${db_authz}    ${robot_prefix}
    Clear Authorization Data From Prefix    ${db_authz}    ${robot_prefix}
    Delete Wemall User From Prefix    ${db}    ${robot_prefix}
    Prepare Data Authorization    ${db_authz}    authorization_search_role_by_shop_user_data.txt
    Register and Map UserId to Shop and Role    ${user_nike_admin}    ${shopName_Nike}    ${role_admin}
    Register and Map UserId to Shop and Role    ${user_nike_admin_noperm}    ${shopName_Nike}    ${role_admin_noperm}
    Register and Map UserId to Shop and Role    ${user_nike_mkt}    ${shopName_Nike}    ${role_marketing}
    ${user_id}    Register and Map UserId to Shop and Role    ${user_nike_prod_mkt}    ${shopName_Nike}    ${role_marketing}
    ${product_role_id}=    Get RoleID by Description    ${db_authz}    ${role_product}
    Mapping Shop&Role&User    ${db_authz}    ${shopName_Nike}    ${product_role_id}    ${user_id}
    Register and Map UserId to Shop and Role    ${user_nike_all}    ${shopName_Nike}    ${role_all}
    Register and Map UserId to Shop and Role    ${user_nb_admin}    ${shopName_Nb}    ${role_admin}
    Register and Map UserId to Shop and Role    ${user_nb_mkt}    ${shopName_Nb}    ${role_marketing}
    Register and Map UserId to Shop and Role    ${user_nb_prod}    ${shopName_Nb}    ${role_product}
    ${user_id}=    Register and Map UserId to Shop and Role    ${user_nike_nb_admin}    ${shopName_Nb}    ${role_admin}
    ${admin_role_id}=    Get RoleID by Description    ${db_authz}    ${role_admin}
    Mapping Shop&Role&User    ${db_authz}    ${shopName_Nike}    ${admin_role_id}    ${user_id}
    Register and Map UserId to Shop and Role    ${user_mkt_all}    ${shopId_all}    ${role_marketing}
    Register and Map UserId to Shop and Role    ${user_allshop_allrole}    ${shopId_all}    ${role_all}
    ${user_id}=    Register and Map UserId to Shop and Role    ${user_nike_noperm_nb_perm}    ${shopName_Nike}    ${role_admin_noperm}
    ${admin_role_id}=    Get RoleID by Description    ${db_authz}    ${role_admin}
    Mapping Shop&Role&User    ${db_authz}    ${shopName_Nb}    ${admin_role_id}    ${user_id}
