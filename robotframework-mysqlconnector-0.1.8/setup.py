#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup
import os

requirements = [
    'tox',
    'coverage',
    'robotframework'
]

test_requirements = [
    # TODO: put package test requirements here
]


def __path(filename):
    return os.path.join(os.path.dirname(__file__), filename)

build = 0

if os.path.exists(__path('build.info')):
    build = open(__path('build.info')).read().strip()

version = '0.1.{}'.format(build)

setup(
    name='robotframework-mysqlconnector',
    version=version,
    description="robotframework-mysqlconnector",
    author="Traitanit Huangsri",
    author_email='traitanit.hua@ascendcorp.com',
    url='http://git.itruemart-dev.com/robotframework/robotframework-mysqlconnector.git',
    packages=[
        'MySQLDBConnector'
    ],
    package_dir={'robotframework-mysqlconnector':
                 'MySQLDBConnector'},
    include_package_data=True,
    install_requires=requirements,
    zip_safe=False,
    keywords='robotframework-mysqlconnector',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: QA',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    test_suite='tests',
    tests_require=test_requirements
)
