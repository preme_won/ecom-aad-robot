# -*- coding: utf-8 -*-

__author__ = 'Traitanit Huangsri'
__email__ = 'traitanit.hua@ascendcorp.com'

from MySQLDBConnectorKeywords import MySQLDBConnectorKeywords

class MySQLDBConnector(MySQLDBConnectorKeywords):
    """MySQLDBConnector is a robotframework keywords to access MySQL database.

    Developed by Traitanit Huangsri

    == How to import library to your robotframework project ==
    You can import this library to your robotframework project like this.

    `resources.robot`
    | *** Settings *** |
    | Library          |  MySQLDBConnector  |

    """

    ROBOT_LIBRARY_SCOPE = "GLOBAL"
    ROBOT_LIBRARY_DOC_FORMAT = "ROBOT"
