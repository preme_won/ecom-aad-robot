# -*- coding: utf-8 -*-

__author__ = 'Traitanit Huangsri'
__email__ = 'traitanit.hua@ascendcorp.com'

from robot.api import logger
import mysql.connector
from robot.api.deco import keyword
class MySQLDBConnectorKeywords(object):
    """
        MySQL driver for robotframework
     """

    @keyword("Connect")
    def connect(self, db_name, db_user, db_password, db_host, db_port=3306):
        """ Connect to Database

        Arguments:
            - db_name: MySQL database name
            - db_user: MySQL database username
            - db_password: MySQL database password
            - db_port: MySQL database port (default=3306)

        Examples:
        | ${db}= | Connect | wemall_aad | root | passwordaad | alpha-aad-db.wemall-dev.com | 3306 |
        """

        try:
            db = mysql.connector.connect(database=db_name, user=db_user, password=db_password, host=db_host,
                                         port=db_port)
        except Exception as ex:
            logger.error(str(ex))
            raise Exception

        return db

    def query(self, db, query_string):
        """ Query data from database

        Arguments:
            - query_string: Query String (Support only single statement)

        Return:
            - Data in List of Dictionary format e.g. [{'ID': 0001, 'Name': 'Robot}, {'ID': 0002, 'Name': 'Test}]

        Examples:
        | ${result}=  | Query | SELECT * FROM VALID_REFRESH_TOKEN |
        """
        result = []
        cursor = db.cursor()
        if db is None:
            logger.error("You're not connecting to the database, please call \'connect\' first")
            raise Exception
        try:
            cursor.execute("SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED")
            cursor.execute(query_string)
            rows = cursor.fetchall()
            if len(rows) == 0:
                return []
            for row in rows:
                result.append(dict(zip(cursor.column_names, row)))
        except Exception as ex:
            print str(ex)
            logger.error(str(ex))
            raise Exception

        return result

    def insert(self, db, sql_statement):
        """ Insert data to database

        Arguments:
            - sql_statement: INSERT INTO SQL statement (Support only single statement)

        Examples:
        | Insert | INSERT INTO valid_refresh_token (user_id ,expiry) VALUES (1114567890 , 1445499788) |
        """
        cursor = db.cursor()
        try:
            if db is None:
                logger.error("You're not connecting to the database, please call \'connect\' first")
                raise Exception
            for result in cursor.execute(sql_statement, multi=True):
                pass

            db.commit()
        except Exception as ex:
            print str(ex)
            logger.error(str(ex))
            raise Exception

    def update(self, db, sql_statement):
        """ Update data on database

        Arguments:
            - sql_statement: UPDATE SQL statement (Support only single statement)

        Examples:
        | Update | UPDATE valid_refresh_token SET expiry=1234567 WHERE user_id = 1114567890  |
        """
        cursor = db.cursor()
        try:
            if db is None:
                logger.error("You're not connecting to the database, please call \'connect\' first")
                return
            cursor.execute("SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED")
            cursor.execute(sql_statement)
            db.commit()
        except Exception as ex:
            logger.error(str(ex))
            raise Exception

    def delete(self, db, sql_statement):
        """ Delete data on database

        Arguments:
            - sql_statement: DELETE SQL statement (Support only single statement)

        Examples:
        | Delete | DELETE from valid_refresh_token WHERE user_id = 1114567890  |
        """
        cursor = db.cursor()
        try:
            if db is None:
                logger.error("You're not connecting to the database, please call \'connect\' first")
                raise Exception
            cursor.execute("SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED")
            cursor.execute(sql_statement)
            db.commit()
        except Exception as ex:
            logger.error(str(ex))
            raise Exception

    @keyword('Close Connection')
    def close_connection(self, db):
        """ Delete data on database

            Arguments:
                - db: database connection object

            Examples:
            | Close Connection | ${db} |
        """
        try:
            if db is None:
                logger.error("You're not connecting to the database, please call \'connect\' first")
                raise Exception
            db.close()
        except Exception as ex:
                logger.error(ex.message)
                raise Exception