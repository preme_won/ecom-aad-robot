import urllib

def decode_url(url):
    return urllib.unquote(url).decode('utf8')

def encode_url(url):
    return urllib.quote(url, safe='').encode('utf8')
