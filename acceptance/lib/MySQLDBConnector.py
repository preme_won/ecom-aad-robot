__author__ = 'Traitanit H.'

import mysql.connector
from robot.api import logger

class MySQLDBConnector(object):
    """
       MySQL driver for robotframework
    """

    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'

    def connect(self, db_name, db_user, db_password, db_host, db_port=3306):
        """ Connect to Database

        Arguments:
            - db_name: MySQL database name
            - db_user: MySQL database username
            - db_password: MySQL database password
            - db_port: MySQL database port (default=3306)

        Examples:
        | ${db}= | Connect | wemall_aad | root | passwordaad | alpha-aad-db.wemall-dev.com | 3306 |
        """

        try:
            db = mysql.connector.connect(database=db_name, user=db_user, password=db_password, host=db_host, port=db_port)
        except Exception as ex:
            logger.error(str(ex))
            raise Exception
        
        return db

    def query(self, db, query_string):
        """ Query data from database

        Arguments:
            - query_string: Query String (Support only single statement)

        Return:
            - Data in List of Dictionary format e.g. [{'ID': 0001, 'Name': 'Robot}, {'ID': 0002, 'Name': 'Test}]

        Examples:
        | ${result}=  | Query | SELECT * FROM VALID_REFRESH_TOKEN |
        """
        result = []
        cursor = db.cursor()
        if db is None:
            logger.error("You're not connecting to the database, please call \'connect\' first")
            raise Exception
        try:
            cursor.execute("SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED")
            cursor.execute(query_string)
            rows = cursor.fetchall()
            if len(rows) == 0:
                return []
            for row in rows:
                result.append(dict(zip(cursor.column_names, row)))
        except Exception as ex:
            print str(ex)
            logger.error(str(ex))
            raise Exception

        return result

    def insert(self, db, sql_statement):
        """ Insert data to database

        Arguments:
            - sql_statement: INSERT INTO SQL statement (Support only single statement)

        Examples:
        | Insert | INSERT INTO valid_refresh_token (user_id ,expiry) VALUES (1114567890 , 1445499788) |
        """
        cursor = db.cursor()
        try:
            if db is None:
                logger.error("You're not connecting to the database, please call \'connect\' first")
                raise Exception
            for result in cursor.execute(sql_statement, multi=True):
                pass

            db.commit()
        except Exception as ex:
            print str(ex)
            logger.error(str(ex))
            raise Exception

    def update(self, db, sql_statement):
        """ Update data on database

        Arguments:
            - sql_statement: UPDATE SQL statement (Support only single statement)

        Examples:
        | Update | UPDATE valid_refresh_token SET expiry=1234567 WHERE user_id = 1114567890  |
        """
        cursor = db.cursor()
        try:
            if db is None:
                logger.error("You're not connecting to the database, please call \'connect\' first")
                return
            cursor.execute("SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED")
            cursor.execute(sql_statement)
            db.commit()
        except Exception as ex:
            logger.error(str(ex))
            raise Exception

    def delete(self, db, sql_statement):
        """ Delete data on database

        Arguments:
            - sql_statement: DELETE SQL statement (Support only single statement)

        Examples:
        | Delete | DELETE from valid_refresh_token WHERE user_id = 1114567890  |
        """
        cursor = db.cursor()
        try:
            if db is None:
                logger.error("You're not connecting to the database, please call \'connect\' first")
                raise Exception
            cursor.execute("SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED")
            cursor.execute(sql_statement)
            db.commit()
        except Exception as ex:
            logger.error(str(ex))
            raise Exception

    def delete_wemall_profile_from_uid_trueid(self, db, uid_of_trueid):
        """ Delete Wemall user's profile data on database

        Arguments:
            - uid_of_trueid: UID of TrueID(After login via TrueID Complete will get UID)

        Examples:
        | Delete Wemall Profile From Uid Trueid | 1114567890  |
        """
        cursor = db.cursor()
        try:
            if db is None:
                logger.error("You're not connecting to the database, please call \'connect\' first")
                raise Exception
            cursor.execute("SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED")
            sql = "select ui.user_id from user_identity ui where ui.identity_id = %(uid)s order by ui.user_id desc"
            cursor.execute(sql, {'uid': uid_of_trueid})
            user_id = cursor.fetchone()

            if not user_id:
                logger.info("Cannot find Wemall User's profile from UID : "+str(uid_of_trueid))
                return
            logger.info("Delete Wemall user-id : "+str(user_id[0]))
            cursor.execute("delete from user where id = %(user_id)s", {'user_id': user_id[0]})
            cursor.execute("delete from user_identity where user_id = %(user_id)s", {'user_id': user_id[0]})
            cursor.execute("delete from valid_refresh_token where user_id = %(user_id)s", {'user_id': user_id[0]})
            db.commit()

        except Exception as ex:
            logger.error(str(ex))
            raise Exception

    def _get_user_id(self, db, prefix):
        cursor = db.cursor()
        try:
            if db is None:
                logger.error("You're not connecting to the database, please call \'connect\' first")
                raise Exception
            cursor.execute("SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED")
            # shopper user_id
            sql = "select s.user_id from shopper s where s.display_name like 'robot%' union " \
                  "select m.user_id from user_password m where m.username like 'robot%';"
            cursor.execute(sql, {'prefix': prefix + "%"})
            rows = cursor.fetchall()
            result = []
            for row in rows:
                result.append(row[0])
            return result
        except Exception as ex:
            logger.error(str(ex))
            raise Exception

    def delete_wemall_user_from_prefix(self, db, prefix):
        """ Delete Wemall user's profile data from prefix email

        Arguments:
            - prefix: prefix of wemall/TrueId email

        Examples:
        | Delete Wemall User From Prefix | robot_aad  |
        """
        cursor = db.cursor()
        try:
            user_id_list = self._get_user_id(db, prefix)
            if len(user_id_list) == 0:
                logger.info("Cannot found user_id from prefix: "+prefix)
                return

            for user_id in user_id_list:
                logger.info("deleting user_id: "+str(user_id))
                cursor.execute('delete from user_identity where user_id = %(user_id)s', {'user_id': user_id})
                cursor.execute('delete from user where id = %(user_id)s', {'user_id': user_id})
                cursor.execute('delete from merchant where user_id = %(user_id)s', {'user_id': user_id})
                cursor.execute('delete from shopper where user_id = %(user_id)s', {'user_id': user_id})
                cursor.execute('delete from user_password where user_id = %(user_id)s', {'user_id': user_id})
                cursor.execute('delete from valid_refresh_token where user_id = %(user_id)s', {'user_id': user_id})
                db.commit()
        except Exception as ex:
            logger.error(str(ex))
            raise Exception


if __name__ == "__main__":
    db = MySQLDBConnector()
    #db_obj = db.connect('wemall_aad', 'root', 'passwordaad', 'alpha-aad-db.wemall-dev.com')
    db_obj = db.connect('wms_a_rds_authz', 'root', 'passwordauthz', 'alpha-authz-db.wemall-dev.com')
    #user_id_list = db._get_user_id(db_obj, 'authen')
    #print str(user_id_list)
    print "count1="+str(db.query(db_obj, 'select count(*) from feature'))
    db.insert(db_obj, 'insert into feature (name, tag) values (\'robot_test3\', \'test3\')')
    print "count2="+str(db.query(db_obj, 'select count(*) from feature'))