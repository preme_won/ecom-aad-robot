import jwt
import base64
from robot.api import logger
from cryptography.x509 import load_pem_x509_certificate
from cryptography.hazmat.backends import default_backend

def decode_payload(encoded ):
    decode_payload  = jwt.decode(encoded, verify=False)
    
    return decode_payload

def decode_header(encoded ):
    decode_header  = jwt.get_unverified_header(encoded)
    
    return decode_header

def verify_signature(encoded ,certificate_file):
    certificate_text = open(certificate_file, 'r').read()
    certificate = load_pem_x509_certificate(certificate_text, default_backend())
    public_key = certificate.public_key()
    logger.info("public key is "+str(public_key))
    decode_publickey = jwt.decode(encoded, public_key, algorithms=['RS256'])

    return decode_publickey

def encoded_Base_64(data):
    encoded = base64.b64encode(data)
    
    return encoded

def decoded_Base_64(data):
    decoded = base64.b64decode(data)
    
    return decoded
