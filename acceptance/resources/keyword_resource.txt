*** Settings ***
Library           RequestsLibrary
Library           Collections
Library           String
Library           DateTime
Library           ExtendedSelenium2Library    browser_breath_delay=2
Library           MySQLDBConnector
Library           OrionAuthLibrary
Variables         ../config/config_staging.yaml
Variables         ../config/test_data.yaml
Variables         ../config/errorCode_errorMessage.yaml

*** Keywords ***
Create Http Basic Auth
    [Arguments]    ${username}    ${password}
    [Documentation]    Create List of username and password for HTTP Basic Authentication
    ${auth}=    Create List    ${username}    ${password}
    [Return]    ${auth}

Convert Dictionary To String
    [Arguments]    ${dict}
    ${json_string}=    Evaluate    json.dumps(${dict})    json
    [Return]    ${json_string}

Create Invalid Signature From Token
    [Arguments]    ${token}
    [Documentation]    Create Invalid Signature From Token.
    ${token_all}=    Split String    ${token}    .
    ${token_header}=    Get From List    ${token_all}    0
    ${token_payload}=    Get From List    ${token_all}    1
    ${token_signature}=    Get From List    ${token_all}    2
    ${token_signature_invalid}=    Catenate    ${token_signature}    ASDF==
    ${token_invalid}=    Catenate    SEPARATOR=.    ${token_header}    ${token_payload}    ${token_signature_invalid}
    [Return]    ${token_invalid}

Response Should Be 200 OK
    [Arguments]    ${response}
    [Documentation]    Expect HTTP 200 [OK]
    Should Be Equal As Integers    ${response.status_code}    200

Response Should Be 201 Created
    [Arguments]    ${response}
    [Documentation]    Expect HTTP 201 [Created]
    Should Be Equal As Integers    ${response.status_code}    201

Response Should Be 400 Bad Request
    [Arguments]    ${response}
    [Documentation]    Expect HTTP 400 [Bad Request]
    Should Be Equal As Integers    ${response.status_code}    400

Response Should Be 401 Unauthorized
    [Arguments]    ${response}
    [Documentation]    Expect HTTP 401 [Unauthorized]
    Should Be Equal As Integers    ${response.status_code}    401

Response Should Be 402 Payment Required
    [Arguments]    ${response}
    [Documentation]    Expect HTTP 402 [Payment Required]
    Should Be Equal As Integers    ${response.status_code}    402

Response Should Be 404 Not Found
    [Arguments]    ${response}
    [Documentation]    Expect HTTP 404 [Not Found]
    Should Be Equal As Integers    ${response.status_code}    404

Response Should Be 500 Internal Server Error
    [Arguments]    ${response}
    [Documentation]    Expect HTTP 500 [Internal Server Error]
    Should Be Equal As Integers    ${response.status_code}    500

Find Merchant Username In Response Text
    [Arguments]    ${response}    ${user_id}
    ${response_json}=    To Json    ${response.text}
    @{merchants}=    Get From Dictionary    ${response_json}    merchants
    : FOR    ${merchant}    IN    @{merchants}
    \    Return From Keyword If    '${merchant['id']}' == '${user_id}'    ${merchant}

Get Header From Token
    [Arguments]    ${Token}
    [Documentation]    Get Header From Token
    ${token_all}=    Split String    ${Token}    .
    ${token_header}=    Get From List    ${token_all}    0
    [Return]    ${token_header}

Get Payload From Token
    [Arguments]    ${Token}
    [Documentation]    Get Payload From Token
    ${token_all}=    Split String    ${Token}    .
    ${token_payload}=    Get From List    ${token_all}    1
    [Return]    ${token_payload}

Get Signature From Token
    [Arguments]    ${Token}
    [Documentation]    Get Signature From Token
    ${token_all}=    Split String    ${Token}    .
    ${token_signature}=    Get From List    ${token_all}    2
    [Return]    ${token_signature}

Get Access Token Epoch Expire Time
    ${time}=    Evaluate    time.time()    time
    ${epoch}    Add Time To Time    ${time}    ${access_token_expire}    exclude_millis=True
    [Return]    ${epoch}

Get Refresh Token Epoch Expire Time
    ${time}=    Evaluate    time.time()    time
    ${epoch}    Add Time To Time    ${time}    ${refresh_token_expire}    exclude_millis=True
    [Return]    ${epoch}

Get Merchant Access Token Epoch Expire Time
    ${time}=    Evaluate    time.time()    time
    ${epoch}    Add Time To Time    ${time}    ${merchant_access_token_expire}    exclude_millis=True
    [Return]    ${epoch}

Get Merchant Refresh Token Epoch Expire Time
    ${time}=    Evaluate    time.time()    time
    ${epoch}    Add Time To Time    ${time}    ${merchant_refresh_token_expire}    exclude_millis=True
    [Return]    ${epoch}

Get JSON Token
    [Arguments]    ${locator}
    ${token}=    Get Text    ${locator}
    ${token}=    Split String    ${token}    separator=:    max_split=1
    ${json}=    To Json    ${token[1]}
    [Return]    ${json}

Get Access Token from Response
    [Arguments]    ${response_text}
    [Documentation]    Get Access Token from Response
    ${json}=    To Json    ${response_text}
    ${access_token}=    Get From Dictionary    ${json}    access_token
    [Return]    ${access_token}

Get Wemall User from Cookie
    [Arguments]    ${token_in_cookie}
    [Documentation]    Get Wemall User from Token in Cookies.
    ...    - Wemall User in access_token&refresh_token must be match.
    ${cookies}=    Fetch From Right    ${token_in_cookie}    ${webui_token_key}=
    ${cookies_all}=    Decode URL    ${cookies}
    ${cookies_all}=    Fetch From Left    ${cookies_all}    ;
    ${cookies_all}=    To Json    ${cookies_all}
    ${access_token_payload}=    Decode Payload    ${cookies_all['access_token']}
    ${userId_wemall_access_token}=    Set Variable    ${access_token_payload['usr']}
    ${refresh_token_payload}=    Decode Payload    ${cookies_all['refresh_token']}
    ${userId_wemall_refresh_token}=    Set Variable    ${refresh_token_payload['usr']}
    Should Be Equal As Numbers    ${userId_wemall_access_token}    ${userId_wemall_refresh_token}
    ${user_id_wemall}=    Set Variable    ${access_token_payload['usr']}
    [Return]    ${user_id_wemall}

Get UID of TrueID
    [Arguments]    ${user_name}    ${passwords}
    [Documentation]    Get UserID of TrueId After Login Complete.
    ${auth}=    Create Http Basic Auth    ${user_name}    ${passwords}
    Create Session    login_trueID    ${trueid_url_staging}    headers=${header}
    ${password_base64}=    Encoded Base 64    ${passwords}
    ${response}=    Post    login_trueID    /rest/oauth2/token    headers=${header}    data=username=${user_name}&client_id=${trueid_clientId}&client_secret=${trueid_clientSecret}&password=${passwords}&grant_type=password&scope=global&state=1
    ${json}=    To Json    ${response.text}
    ${access_token}=    Get From Dictionary    ${json}    access_token
    #Get uid's TrueId
    Create Session    get_profile    ${trueid_url_getProfile}
    ${trueID_profile}=    Get    get_profile    /profile/me?access_token=${access_token}
    ${trueID_profile}=    To Json    ${trueID_profile.text}
    ${response_trueID}=    Set Variable    ${trueID_profile['uid']}
    [Return]    ${response_trueID}

Get Token from Cookie
    [Arguments]    ${token_in_cookie}
    [Documentation]    Get Token from Cookies.Will Return
    ...    - access_token
    ...    - refresh_token
    ${cookies}=    Fetch From Right    ${token_in_cookie}    ${webui_token_key}=
    ${cookies_all}=    Decode URL    ${cookies}
    ${cookies_all}=    Fetch From Left    ${cookies_all}    ;
    ${cookies_all}=    To Json    ${cookies_all}
    [Return]    ${cookies_all}    # return access_token, refresh_token

Get Merchant Info
    [Arguments]    ${db}    ${user_id}
    ${result}=    Query    ${db}    select * from merchant m, user_password p, user u, user_identity i where m.user_id = ${user_id} and m.user_id = p.user_id and m.user_id = u.id and m.user_id = i.user_id
    ${length}=    Evaluate    len(${result})
    Should Be Equal As Integers    ${length}    1
    [Return]    ${result[0]}

Get Refresh Token from Response
    [Arguments]    ${response_text}
    [Documentation]    Get Refresh Token from Response
    ${json}=    To Json    ${response_text}
    ${refresh_token}=    Get From Dictionary    ${json}    refresh_token
    [Return]    ${refresh_token}

Get Visible Error Message
    [Arguments]    ${error_msg_locator}
    [Documentation]    Get Token from Cookies.Will Return
    ...    - access_token
    ...    - refresh_token
    @{error_msg}=    Get Webelements    ${error_msg_locator}
    @{actual_msg}=    Create List
    : FOR    ${element}    IN    @{error_msg}
    \    Run Keyword If    '${element.text}' != '${EMPTY}'    Append To List    ${actual_msg}    ${element.text}
    [Return]    ${actual_msg}    # return error message

Get Shopper Info
    [Arguments]    ${db}    ${user_id}
    ${result}=    Query    ${db}    select * from shopper s, user u, user_identity i where s.user_id = u.id and s.user_id = i.user_id and s.user_id = ${user_id}
    ${length}=    Evaluate    len(${result})
    Should Be Equal As Integers    ${length}    1
    [Return]    ${result[0]}

Generate Email with epoch
    [Arguments]    ${prefix}    ${domain_name}
    [Documentation]    Return identity_id with prefix and append epoch time
    ${epoch}=    Get Current Date    result_format=epoch    exclude_millis=False
    ${epoch}=    Convert To String    ${epoch}
    ${epoch}=    Convert Number To Alphabet    ${epoch}
    ${user}=    Catenate    SEPARATOR=_    ${prefix}    ${epoch}
    ${user}=    Catenate    SEPARATOR=@    ${user}    ${domain_name}
    [Return]    ${user}

Login to Wemall(API)
    [Arguments]    ${uri}    ${user_name}    ${passwords}    ${data}
    [Documentation]    Login to WeMall with API and Return Response Message
    ${auth}=    Create Http Basic Auth    ${user_name}    ${passwords}
    Create Session    login    ${context_root}    headers=${header}    auth=${auth}
    ${response_msg}=    Post Request    login    ${uri}    headers=${header}    data=${data}
    [Return]    ${response_msg}

Number of Merchant Search Result Should Be
    [Arguments]    ${response}    ${number}
    ${response_json}=    To Json    ${response.text}
    ${merchants}=    Get From Dictionary    ${response_json}    merchants
    ${length}=    Get Length    ${merchants}
    Should Be Equal As Numbers    ${length}    ${number}

Open Browser
    Create Webdriver    Chrome  # ${webui_browser}

Open Browser and Maximize Window
    [Arguments]    ${url}    ${token_key}=${webui_token_key}
    Run Keyword If    '${webui_browser}' == 'phantomjs'    Open PhantomJS Browser
    Run Keyword If    '${webui_browser}' != 'phantomjs'    Open Browser
    Maximize Browser Window
    Delete All Cookies
    Go To    ${url}
    Wait Until Angular Ready    ${webui_selenium_timeout}
    ${cookies}=    Get Cookies
    Should Not Contain    ${cookies}    ${token_key}

Open PhantomJS Browser
    ${service_args}=    Create List    --ignore-ssl-errors=true
    Create Webdriver    PhantomJS    service_args=${service_args}

Revoke Token(API)
    [Arguments]    ${uri}    ${data}
    [Documentation]    Revoke Token(API) and Return Response Message
    Create Session    login    ${context_root}    headers=${header}
    ${response_msg}=    Post Request    login    ${uri}    headers=${header}    data=${data}
    [Return]    ${response_msg}

Refresh Token(API)
    [Arguments]    ${uri}    ${data}
    [Documentation]    Refresh Token with API and Return Response Message
    Create Session    refresh    ${context_root}    headers=${header}
    ${response_msg}=    Post Request    refresh    ${uri}    headers=${header}    data=${data}
    [Return]    ${response_msg}

Register to Wemall(API)
    [Arguments]    ${username}    ${password}    ${confirm_password}    ${display_name}    ${identity_type}=trueid
    [Documentation]    Register a new wemall user, then return HTTP response object
    ${data}=    Catenate    SEPARATOR=&    identity_type=${identity_type}    email=${username}    display_name=${display_name}    password=${password}
    ...    password_confirm=${confirm_password}
    Create Session    Register    url=${context_root}    headers=${header}
    ${response}=    Post Request    Register    ${register_uri}    data=${data}
    [Return]    ${response}

Register Wemall User
    [Arguments]    ${password}
    [Documentation]    Register Wemall User and Return Wemall's User after Created.
    ...    - Wemall User is auto generator. Ex 'robot_aad_1443776382@wemall-dev.com'
    ${username}=    Generate Email with epoch    ${register_user_prefix}    ${register_domain_name}
    ${response}=    Register to Wemall(API)    ${username}    ${password}    ${password}    ${username}
    Response Should Be 201 Created    ${response}
    [Return]    ${username}

Register merchant
    [Arguments]    ${username}    ${email}    ${first_name}    ${last_name}    ${password}    ${password_confirm}
    [Documentation]    Register new merchant
    ${data}=    Catenate    SEPARATOR=&    username=${username}    email=${email}    first_name=${first_name}    last_name=${last_name}
    ...    password=${password}    password_confirm=${password_confirm}
    Create Session    Register Merchant    url=${context_root}    headers=${header}
    ${response}=    Post Request    Register Merchant    ${register_merchant_uri}    data=${data}
    [Return]    ${response}

Suite Setup
    ${db}=    Connect    ${database_name}    ${database_username}    ${database_password}    ${database_host}    ${database_port}
    Set Suite Variable    ${db}    ${db}
    Delete Wemall User From Prefix    ${db}    ${register_user_prefix}

Search Merchant
    [Arguments]    ${query_string}
    Create Session    Search Merchant    url=${context_root}
    ${response}=    Get Request    Search Merchant    ${search_merchant_uri}    params=${query_string}
    [Return]    ${response}

Should Not Found Merchant
    [Arguments]    ${response}
    ${json}=    To Json    ${response.text}
    @{merchants}=    Get From Dictionary    ${json}    merchants
    ${length}=    Get Length    ${merchants}
    Should Be Equal As Numbers    ${length}    0

Validate Login Completed
    [Arguments]    ${response_msg}
    [Documentation]    If Login complete ,Response Message Should Contain
    ...    * access_token
    ...    * refresh_token
    Response Should Be 200 OK    ${response_msg}
    Should Contain    ${response_msg.content}    refresh_token
    Should Contain    ${response_msg.content}    access_token

Validate Access Token with Publickey
    [Arguments]    ${access_token_encode}    ${expect_user_type}
    [Documentation]    Validate Access Token With Publickey
    ${access_token_decode}=    Decode Payload    ${access_token_encode}
    ${access_token_key_length}=    Get Length    ${access_token_decode}
    Should Be Equal As Numbers    ${access_token_key_length}    4
    Verify Merchant Access Token Expire Date    ${access_token_decode['exp']}
    Should Be Equal As Strings    ${access_token_decode['typ']}    ${expect_user_type}
    Verify Signature of Token    ${access_token_encode}    ${access_publickey_certificate}

Validate Refresh Token with Publickey
    [Arguments]    ${refresh_token_encode}    ${expect_user_type}
    [Documentation]    Validate refresh Token With Publickey
    ${refresh_token_decode}=    Decode Payload    ${refresh_token_encode}
    ${refresh_token_key_length}=    Get Length    ${refresh_token_decode}
    Should Be Equal As Numbers    ${refresh_token_key_length}    3
    Verify Merchant Refresh Token Expire Date    ${refresh_token_decode['exp']}
    Should Be Equal As Strings    ${refresh_token_decode['typ']}    ${expect_user_type}
    Verify Signature of Token    ${refresh_token_encode}    ${refresh_publickey_certificate}

Validate Merchant Access Token with Publickey
    [Arguments]    ${access_token_encode}    ${expect_user_type}
    [Documentation]    Validate Access Token With Publickey
    ${access_token_decode}=    Decode Payload    ${access_token_encode}
    ${access_token_key_length}=    Get Length    ${access_token_decode}
    Should Be Equal As Numbers    ${access_token_key_length}    4
    Verify Access Token Expire Date    ${access_token_decode['exp']}
    Should Be Equal As Strings    ${access_token_decode['typ']}    ${expect_user_type}
    Verify Signature of Token    ${access_token_encode}    ${access_publickey_certificate}

Validate Merchant Refresh Token with Publickey
    [Arguments]    ${refresh_token_encode}    ${expect_user_type}
    [Documentation]    Validate refresh Token With Publickey
    ${refresh_token_decode}=    Decode Payload    ${refresh_token_encode}
    ${refresh_token_key_length}=    Get Length    ${refresh_token_decode}
    Should Be Equal As Numbers    ${refresh_token_key_length}    3
    Verify Refresh Token Expire Date    ${refresh_token_decode['exp']}
    Should Be Equal As Strings    ${refresh_token_decode['typ']}    ${expect_user_type}
    Verify Signature of Token    ${refresh_token_encode}    ${refresh_publickey_certificate}

Validate Wemall userID with TrueID
    [Arguments]    ${db}    ${user_id_wemall}    ${uid_trueId}
    [Documentation]    Validate Wemall User-ID with TrueID.System must be return "Wemall User-ID"
    Should Not Be Equal As Numbers    ${user_id_wemall}    ${uid_trueId}
    #Wemall user of TrueID won't duplicate
    ${result}=    Query    ${db}    select count(u.id) as record from user u inner join user_identity ui where u.id = ui.user_id and identity_type = 'TRUEID' and ui.identity_id = ${uid_trueId} and ui.user_id = ${user_id_wemall}
    ${data_dict}=    Get From List    ${result}    0
    ${data_count}=    Get From Dictionary    ${data_dict}    record
    Should Be Equal As Numbers    ${data_count}    1
    #Verify Wemall UserID from DB
    ${result_wemall}=    Query    ${db}    select u.id as wemall_userID from user u inner join user_identity ui where u.id = ui.user_id and identity_type = 'TRUEID' and ui.identity_id = ${uid_trueId}
    ${data_dict_wemall}=    Get From List    ${result_wemall}    0
    ${wemall_userID_DB}=    Get From Dictionary    ${data_dict_wemall}    wemall_userID
    #Verify System return Wemall userID correct
    Should Be Equal As Numbers    ${wemall_userID_DB}    ${user_id_wemall}

Validate Refresh After Login Complete
    [Arguments]    ${db}    ${user_id_wemall}    ${refresh_token}
    [Documentation]    Validate After Login to Wemall Complete ,refresh_token must be store in DB.
    #Refresh Token must be store on DB and won't duplicate
    ${refresh_token_payload}=    Decode Payload    ${refresh_token}
    ${refresh_token_exp}=    Set Variable    ${refresh_token_payload['exp']}
    ${result}=    Query    ${db}    select count(vrt.user_id) as record from valid_refresh_token vrt where vrt.user_id = ${user_id_wemall} and vrt.expiry = ${refresh_token_exp}
    ${data_dict}=    Get From List    ${result}    0
    ${data_count}=    Get From Dictionary    ${data_dict}    record
    Should Be Equal As Numbers    ${data_count}    1

Validate Admin Token in Cookie
    [Arguments]    ${db}    ${token_in_cookies}    ${user_type}    ${expect_merchant_dnm}
    [Documentation]    Validate Admin Token in Cookie Must be Contain 1.access_token 2.refresh_token ,Then validate with publickey.
    ${cookies}=    Fetch From Right    ${token_in_cookies}    ${webui_admin_token_key}=
    ${cookies_all}=    Decode URL    ${cookies}
    ${cookies_all}=    To Json    ${cookies_all}
    Validate Access Token with Publickey    ${cookies_all['access_token']}    ${user_type}
    ${access_token_payload}=    Decode Payload    ${cookies_all['access_token']}
    Should Be Equal As Strings    ${access_token_payload['dnm']}    ${expect_merchant_dnm}
    Validate Refresh Token with Publickey    ${cookies_all['refresh_token']}    ${user_type}
    ${user_id_wemall}=    Set Variable    ${access_token_payload['usr']}
    Validate Refresh After Login Complete    ${db}    ${user_id_wemall}    ${cookies_all['refresh_token']}

Validate Token in Cookie
    [Arguments]    ${db}    ${token_in_cookies}    ${user_type}
    [Documentation]    Validate Token in Cookie Must be Contain 1.access_token 2.refresh_token ,Then validate with publickey.
    ${cookies}=    Fetch From Right    ${token_in_cookies}    ${webui_token_key}=
    ${cookies_all}=    Decode URL    ${cookies}
    ${cookies_all}=    Fetch From Left    ${cookies_all}    ;
    ${cookies_all}=    To Json    ${cookies_all}
    Validate Access Token with Publickey    ${cookies_all['access_token']}    ${user_type}
    ${access_token_payload}=    Decode Payload    ${cookies_all['access_token']}
    Validate Refresh Token with Publickey    ${cookies_all['refresh_token']}    ${user_type}
    ${user_id_wemall}=    Set Variable    ${access_token_payload['usr']}
    Validate Refresh After Login Complete    ${db}    ${user_id_wemall}    ${cookies_all['refresh_token']}

Verify Json Response
    [Arguments]    ${JsonObject}    ${key}    ${expect_value}
    ${value}=    Get From Dictionary    ${JsonObject}    ${key}
    Should Be Equal As Strings    ${value}    ${expect_value}

Verify Access Token Expire Date
    [Arguments]    ${access_token_exp}
    [Documentation]    Delay between server and client should not more than 5 seconds
    ${next_hour_epoch}=    Get Access Token Epoch Expire Time
    ${diff}=    Evaluate    ${next_hour_epoch} - ${access_token_exp}    math
    ${result}=    Evaluate    ${diff} <= ${token_expire_offset}    math
    Should Be True    ${result}

Verify Merchant Access Token Expire Date
    [Arguments]    ${access_token_exp}
    [Documentation]    Delay between server and client should not more than 5 seconds
    ${next_hour_epoch}=    Get Merchant Access Token Epoch Expire Time
    ${diff}=    Evaluate    ${next_hour_epoch} - ${access_token_exp}    math

    Log To Console   diff=${diff}
    Log To Console   token_expire_offset=${token_expire_offset}

    ${result}=    Evaluate    ${diff} <= ${token_expire_offset}    math

    Should Be True    ${result}

Verify Signature of Token
    [Arguments]    ${token}    ${public_key}
    ${certificate_file}=    Catenate    SEPARATOR=${/}    ${CURDIR}    ${public_key}
    Verify Signature    ${token}    ${certificate_file}

Verify Refresh Token Expire Date
    [Arguments]    ${refresh_token_exp}
    [Documentation]    Delay between server and client should not more than 5 seconds
    ${next_hour_epoch}=    Get Refresh Token Epoch Expire Time
    ${diff}=    Evaluate    ${next_hour_epoch} - ${refresh_token_exp}    math
    ${result}=    Evaluate    ${diff} <= ${token_expire_offset}    math
    Should Be True    ${result}

Verify Merchant Refresh Token Expire Date
    [Arguments]    ${refresh_token_exp}
    [Documentation]    Delay between server and client should not more than 5 seconds
    ${next_hour_epoch}=    Get Merchant Refresh Token Epoch Expire Time
    ${diff}=    Evaluate    ${next_hour_epoch} - ${refresh_token_exp}    math
    ${result}=    Evaluate    ${diff} <= ${token_expire_offset}    math
    Should Be True    ${result}

Verify ErrorCode and ErrorMsg From Response
    [Arguments]    ${json_response}    ${expect_errorCode}    ${expect_errorMessage}
    ${json}=    To Json    ${json_response}
    Dictionary Should Contain Item    ${json}    errorCode    ${expect_errorCode}
    Dictionary Should Contain Item    ${json}    errorMessage    ${expect_errorMessage}

Verify Registration Info
    [Arguments]    ${db}    ${user_id}    ${display_name}
    ${shopper}=    Get Shopper Info    ${db}    ${user_id}
    Should Be Equal As Strings    ${shopper['type']}    SHOPPER
    Should Be Equal As Strings    ${shopper['display_name']}    ${display_name}
    Should Be Equal As Strings    ${shopper['identity_type']}    TRUEID
    Should Not Be Empty    ${shopper['identity_id']}

Verify TrueId Error Message
    [Arguments]    ${response}    ${error_code}    ${error_message}
    ${json}=    To Json    ${response.text}
    ${error_message}=    Convert To String    ${error_message}
    Should Be Equal As Strings    ${json['errorCode']}    ${error_code}
    Should Be Equal As Strings    ${json['errorMessage']}    ${error_message}

Verify Merchant Registration Info
    [Arguments]    ${db}    ${user_id}    ${username}    ${email}    ${first_name}    ${last_name}
    ${merchant}=    Get Merchant Info    ${db}    ${user_id}
    Should Be Equal As Strings    ${merchant['first_name']}    ${first_name}
    Should Be Equal As Strings    ${merchant['last_name']}    ${last_name}
    Should Be Equal As Strings    ${merchant['email']}    ${email}
    Should Be Equal As Strings    ${merchant['username']}    ${username}
    Should Be Equal As Strings    ${merchant['type']}    MERCHANT
    Should Not Be Empty    ${merchant['password']}
    Should Not Be Empty    ${merchant['salt']}
    Should Be Equal As Strings    ${merchant['identity_type']}    WEMALL
    Should Not Be Empty    ${merchant['identity_id']}

Verify Search Merchant Response
    [Arguments]    ${merchant_json}    ${id}    ${username}    ${first_name}    ${last_name}    ${email}
    Should Be Equal As Strings    ${merchant_json['id']}    ${id}
    Should Be Equal As Strings    ${merchant_json['type']}    MERCHANT
    Should Be Equal As Strings    ${merchant_json['username']}    ${username}
    Should Be Equal As Strings    ${merchant_json['firstName']}    ${first_name}
    Should Be Equal As Strings    ${merchant_json['lastName']}    ${last_name}
    Should Be Equal As Strings    ${merchant_json['email']}    ${email}

User Id must be returned in ${response}
    ${json}=    To Json    ${response.text}
    Should Not Be Equal As Integers    ${json['user_id']}    0
    Return From Keyword    ${json['user_id']}
