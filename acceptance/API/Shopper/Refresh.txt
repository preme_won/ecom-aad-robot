*** Settings ***
Suite Setup       Suite Setup
Test Setup        Delete Wemall User From Prefix    ${db}    ${register_user_prefix}
Test Teardown     Delete All Sessions
Force Tags        ready    regression    authentication-api    refreshToken
Resource          ../../resources/keyword_resource.txt

*** Test Cases ***
TC_WeMall_00006
    [Documentation]    Customer(True ID) get token when Login ,Then Request New Token before access_token Expire.
    #Create Wemall User
    ${username}=    Register Wemall User    ${password}
    #Login with 'True ID'
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${username}    ${password}    grant_type=trueid
    Validate Login Completed    ${response}
    ${access_token}=    Get Access Token from Response    ${response.text}
    ${refresh_token}=    Get Refresh Token from Response    ${response.text}
    Validate Access Token with Publickey    ${access_token}    ${typ_shopper}
    ${access_token_payload}=    Decode Payload    ${access_token}
    ${userId}=    Set Variable    ${access_token_payload['usr']}
    Validate Refresh Token with Publickey    ${refresh_token}    ${typ_shopper}
    #Request New Token before access_token Expire
    Delete All Sessions
    ${response}=    Refresh Token(API)    ${create_token_uri}    grant_type=refresh_token&refresh_token=${refresh_token}
    Validate Login Completed    ${response}
    ${access_token}=    Get Access Token from Response    ${response.text}
    ${refresh_token}=    Get Refresh Token from Response    ${response.text}
    Validate Access Token with Publickey    ${access_token}    ${typ_shopper}
    ${access_token_payload}=    Decode Payload    ${access_token}
    ${userId}=    Set Variable    ${access_token_payload['usr']}
    Validate Refresh Token with Publickey    ${refresh_token}    ${typ_shopper}

TC_WeMall_00007
    [Documentation]    Customer(True ID) Request New Token Without Attach 'Refresh Token'.
    #Create Wemall User
    ${username}=    Register Wemall User    ${password}
    #Login with 'True ID'
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${username}    ${password}    grant_type=trueid
    Validate Login Completed    ${response}
    ${access_token}=    Get Access Token from Response    ${response.text}
    ${refresh_token}=    Get Refresh Token from Response    ${response.text}
    Validate Access Token with Publickey    ${access_token}    ${typ_shopper}
    ${access_token_payload}=    Decode Payload    ${access_token}
    ${userId}=    Set Variable    ${access_token_payload['usr']}
    Validate Refresh Token with Publickey    ${refresh_token}    ${typ_shopper}
    #Request New Token w/o Attach 'Refresh Token'
    Delete All Sessions
    ${response}=    Refresh Token(API)    ${create_token_uri}    grant_type=refresh_token
    Response Should Be 400 Bad Request    ${response}
    Verify ErrorCode and ErrorMsg From Response    ${response.text}    ${errorCode_invalid_param}    ${errorMessage_refresh_token_must_not_blank}

TC_WeMall_00008
    [Documentation]    Customer(True ID) Request New Token With Invalid 'Refresh Token'.
    #Create Wemall User
    ${username}=    Register Wemall User    ${password}
    #Login with 'True ID'
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${username}    ${password}    grant_type=trueid
    Validate Login Completed    ${response}
    ${access_token}=    Get Access Token from Response    ${response.text}
    ${refresh_token}=    Get Refresh Token from Response    ${response.text}
    Validate Access Token with Publickey    ${access_token}    ${typ_shopper}
    ${access_token_payload}=    Decode Payload    ${access_token}
    ${userId}=    Set Variable    ${access_token_payload['usr']}
    Validate Refresh Token with Publickey    ${refresh_token}    ${typ_shopper}
    ##Prepare Invalid Signature of 'Refresh Token'
    ${refresh_token_invalid}=    Create Invalid Signature From Token    ${refresh_token}
    #Request New Token With Invalid 'Refresh Token'
    Delete All Sessions
    ${response}=    Refresh Token(API)    ${create_token_uri}    grant_type=refresh_token&refresh_token=${refresh_token_invalid}
    Response Should Be 401 Unauthorized    ${response}
    Verify ErrorCode and ErrorMsg From Response    ${response.text}    ${errorCode_unauthorized_request}    ${errorMessage_refresh_token_signature_invalid}
