*** Settings ***
Suite Setup       Suite Setup
Test Setup        Delete Wemall User From Prefix    ${db}    ${register_user_prefix}
Test Teardown     Delete All Sessions
Force Tags        ready    regression    authentication-api    login-merchant
Resource          ../../resources/keyword_resource.txt

*** Test Cases ***
TC_WeMall_00076
    [Documentation]    Verify that merchant-user is able to login with valid username and password.
    #Register Wemall's merchant User
    [Tags]   login
    ${response}=    Register merchant    username=${register_merchant_username}    email=${register_merchant_email}    first_name=${register_merchant_first_name}    last_name=${register_merchant_last_name}    password=${password}
    ...    password_confirm=${password}
    Response Should Be 201 Created    ${response}
    ${user_id}=    User Id must be returned in ${response}
    Verify Merchant Registration Info    ${db}    ${user_id}    ${register_merchant_username}    ${register_merchant_email}    ${register_merchant_first_name}    ${register_merchant_last_name}
    #Login with Wemall's merchant User
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${register_merchant_username}    ${password}    grant_type=merchant
    Validate Login Completed    ${response}
    ${access_token}=    Get Access Token from Response    ${response.text}
    ${refresh_token}=    Get Refresh Token from Response    ${response.text}
    Validate Access Token with Publickey    ${access_token}    ${typ_merchant}
    ${access_token_payload}=    Decode Payload    ${access_token}
    ${userId_wemall}=    Set Variable    ${access_token_payload['usr']}
    Validate Refresh Token with Publickey    ${refresh_token}    ${typ_merchant}
    #Dnm of Merchant will be 'firstName lastName'
    ${expect_merc_dnm}=    Catenate    SEPARATOR=${SPACE}    ${register_merchant_first_name}    ${register_merchant_last_name}
    Should Be Equal As Strings    ${access_token_payload['dnm']}    ${expect_merc_dnm}

TC_WeMall_00077
    [Documentation]    Verify that merchant-user is not able to login with invalid username/password.
    #Register Wemall's merchant User
    ${response}=    Register merchant    username=${register_merchant_username}    email=${register_merchant_email}    first_name=${register_merchant_first_name}    last_name=${register_merchant_last_name}    password=${password}
    ...    password_confirm=${password}
    Response Should Be 201 Created    ${response}
    ${user_id}=    User Id must be returned in ${response}
    Verify Merchant Registration Info    ${db}    ${user_id}    ${register_merchant_username}    ${register_merchant_email}    ${register_merchant_first_name}    ${register_merchant_last_name}
    #1. Login with invalid USER
    ${response}=    Login to Wemall(API)    ${create_token_uri}    Invalid${register_merchant_username}    ${password}    grant_type=merchant
    Response Should Be 401 Unauthorized    ${response}
    Verify ErrorCode and ErrorMsg From Response    ${response.text}    ${errorCode_unauthorized_request}    ${errorMessage_invalid_username_or_password}
    #2. Login with invalid PASSWORD
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${register_merchant_username}    Invalid${password}    grant_type=merchant
    Response Should Be 401 Unauthorized    ${response}
    Verify ErrorCode and ErrorMsg From Response    ${response.text}    ${errorCode_unauthorized_request}    ${errorMessage_invalid_username_or_password}
    #3. Login with invalid USER and PASSWORD
    ${response}=    Login to Wemall(API)    ${create_token_uri}    Invalid${register_merchant_username}    Invalid${password}    grant_type=merchant
    Response Should Be 401 Unauthorized    ${response}
    Verify ErrorCode and ErrorMsg From Response    ${response.text}    ${errorCode_unauthorized_request}    ${errorMessage_invalid_username_or_password}

TC_WeMall_00078
    [Documentation]    Verify that validation error message in case user leaves username or password field as blank.
    #Register Wemall's merchant User
    ${response}=    Register merchant    username=${register_merchant_username}    email=${register_merchant_email}    first_name=${register_merchant_first_name}    last_name=${register_merchant_last_name}    password=${password}
    ...    password_confirm=${password}
    Response Should Be 201 Created    ${response}
    ${user_id}=    User Id must be returned in ${response}
    Verify Merchant Registration Info    ${db}    ${user_id}    ${register_merchant_username}    ${register_merchant_email}    ${register_merchant_first_name}    ${register_merchant_last_name}
    #1. Login with blank USER
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${EMPTY}    ${password}    grant_type=merchant
    Response Should Be 401 Unauthorized    ${response}
    Verify ErrorCode and ErrorMsg From Response    ${response.text}    ${errorCode_unauthorized_request}    ${errorMessage_invalid_username_or_password}
    #2. Login with blank PASSWORD
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${register_merchant_username}    ${EMPTY}    grant_type=merchant
    Response Should Be 401 Unauthorized    ${response}
    Verify ErrorCode and ErrorMsg From Response    ${response.text}    ${errorCode_unauthorized_request}    ${errorMessage_invalid_username_or_password}
    #3. Login with blank USER and PASSWORD
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${EMPTY}    ${EMPTY}    grant_type=merchant
    Response Should Be 401 Unauthorized    ${response}
    Verify ErrorCode and ErrorMsg From Response    ${response.text}    ${errorCode_unauthorized_request}    ${errorMessage_invalid_username_or_password}

TC_WeMall_00080
    [Documentation]    Merchant can't authenticated with Shopper's username/password.
    #Create Wemall User
    ${username}=    Register Wemall User    ${password}
    #1. Login with Shopper's user&password
    ${response}=    Login to Wemall(API)    ${create_token_uri}    ${username}    ${password}    grant_type=merchant
    Response Should Be 401 Unauthorized    ${response}
    Verify ErrorCode and ErrorMsg From Response    ${response.text}    ${errorCode_unauthorized_request}    ${errorMessage_invalid_username_or_password}
