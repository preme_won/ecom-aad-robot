*** Settings ***
Documentation     Search Merchant API
Suite Setup       Suite Setup
Test Setup        Delete Merchants
Default Tags      ready    regression    authentication-api    search-merchant
Resource          ../../resources/keyword_resource.txt

*** Test Cases ***
TC_WeMall_00120
    [Documentation]    Exact search merchant by username
    # Register Merchant
    ${response}=    Register merchant    username=${register_merchant_username}    email=${register_merchant_email}    first_name=${register_merchant_first_name}    last_name=${register_merchant_last_name}    password=${password}
    ...    password_confirm=${password}
    Response Should Be 201 Created    ${response}
    ${user_id}=    User Id must be returned in ${response}
    ${query_string}=    Create Dictionary    username=${register_merchant_username}
    # Search Merchant
    ${response}=    Search Merchant    ${query_string}
    Response Should Be 200 OK    ${response}
    # Verify with Expected Result
    Number of Merchant Search Result Should Be    ${response}    1
    ${merchant_json}=    Find Merchant Username In Response Text    ${response}    ${user_id}
    Verify Search Merchant Response    ${merchant_json}    ${user_id}    ${register_merchant_username}    ${register_merchant_first_name}    ${register_merchant_last_name}    ${register_merchant_email}

TC_WeMall_00121
    [Documentation]    Exact search merchant by user_id
    # Register Merchant
    ${response}=    Register merchant    username=${register_merchant_username}    email=${register_merchant_email}    first_name=${register_merchant_first_name}    last_name=${register_merchant_last_name}    password=${password}
    ...    password_confirm=${password}
    Response Should Be 201 Created    ${response}
    ${user_id}=    User Id must be returned in ${response}
    ${query_string}=    Create Dictionary    userId=${user_id}
    # Search Merchant
    ${response}=    Search Merchant    ${query_string}
    Response Should Be 200 OK    ${response}
    # Verify Expected Result
    Number of Merchant Search Result Should Be    ${response}    1
    ${merchant_json}=    Find Merchant Username In Response Text    ${response}    ${user_id}
    Verify Search Merchant Response    ${merchant_json}    ${user_id}    ${register_merchant_username}    ${register_merchant_first_name}    ${register_merchant_last_name}    ${register_merchant_email}

TC_WeMall_00122
    [Documentation]    Search with non-existing username
    # Search Merchant
    ${response}=    Search Merchant    username=${register_merchant_username}
    Response Should Be 200 OK    ${response}
    Should Not Found Merchant    ${response}

TC_WeMall_00123
    [Documentation]    Search without query string
    ${expect_num_users}=    Count All Unique Merchant User    ${db}
    Create Session    Search Merchant    url=${context_root}
    ${response}=    Get Request    Search Merchant    ${search_merchant_uri}
    Response Should Be 200 OK    ${response}
    Number of Merchant Search Result Should Be    ${response}    ${expect_num_users}

TC_WeMall_00124
    [Documentation]    Batch search with userId
    # Register Merchant 1
    ${response}=    Register merchant    username=${register_merchant_username}    email=${register_merchant_email}    first_name=${register_merchant_first_name}    last_name=${register_merchant_last_name}    password=${password}
    ...    password_confirm=${password}
    Response Should Be 201 Created    ${response}
    ${user_id_1}=    User Id must be returned in ${response}
    ${response}=    Register merchant    username=${register_merchant_username}_02    email=${register_merchant_email}    first_name=${register_merchant_first_name}    last_name=${register_merchant_last_name}    password=${password}
    ...    password_confirm=${password}
    Response Should Be 201 Created    ${response}
    ${user_id_2}=    User Id must be returned in ${response}
    ${query_string}=    Create Dictionary    userId=${user_id_1},${user_id_2}
    # Search Merchant
    ${response}=    Search Merchant    ${query_string}
    Response Should Be 200 OK    ${response}
    Number of Merchant Search Result Should Be    ${response}    2
    # Verify Expected Result for user_id_1
    ${merchant_json}=    Find Merchant Username In Response Text    ${response}    ${user_id_1}
    Verify Search Merchant Response    ${merchant_json}    ${user_id_1}    ${register_merchant_username}    ${register_merchant_first_name}    ${register_merchant_last_name}    ${register_merchant_email}
    # Verify Expected Result for user_id_2
    ${merchant_json}=    Find Merchant Username In Response Text    ${response}    ${user_id_2}
    Verify Search Merchant Response    ${merchant_json}    ${user_id_2}    ${register_merchant_username}_02    ${register_merchant_first_name}    ${register_merchant_last_name}    ${register_merchant_email}

TC_WeMall_00148
    [Documentation]    Batch search merchant with some non-existing userId
    # Register Merchant
    ${response}=    Register merchant    username=${register_merchant_username}    email=${register_merchant_email}    first_name=${register_merchant_first_name}    last_name=${register_merchant_last_name}    password=${password}
    ...    password_confirm=${password}
    Response Should Be 201 Created    ${response}
    ${user_id}=    User Id must be returned in ${response}
    ${query_string}=    Create Dictionary    userId=${user_id},-1,0,1
    # Search Merchant
    ${response}=    Search Merchant    ${query_string}
    Response Should Be 200 OK    ${response}
    Number of Merchant Search Result Should Be    ${response}    1
    # Verify Expected Result
    ${merchant_json}=    Find Merchant Username In Response Text    ${response}    ${user_id}
    Verify Search Merchant Response    ${merchant_json}    ${user_id}    ${register_merchant_username}    ${register_merchant_first_name}    ${register_merchant_last_name}    ${register_merchant_email}

TC_WeMall_00149
    [Documentation]    Batch search merchant with an incorrect query string
    # Register Merchant
    ${response}=    Register merchant    username=${register_merchant_username}    email=${register_merchant_email}    first_name=${register_merchant_first_name}    last_name=${register_merchant_last_name}    password=${password}
    ...    password_confirm=${password}
    Response Should Be 201 Created    ${response}
    ${user_id}=    User Id must be returned in ${response}
    ${query_string}=    Create Dictionary    userId=${register_merchant_username}
    # Search Merchant with userId=${username}
    ${response}=    Search Merchant    ${query_string}
    Response Should Be 200 OK    ${response}
    Should Not Found Merchant    ${response}
    ${query_string}=    Create Dictionary    username=${user_id}
    # Search Merchant with username=${userId}
    ${response}=    Search Merchant    ${query_string}
    Response Should Be 200 OK    ${response}
    Should Not Found Merchant    ${response}

TC_WeMall_00150
    [Documentation]    Search sub string of username
    # Register Merchant
    [Tags]   xxx
    ${response}=    Register merchant    username=${register_merchant_username}    email=${register_merchant_email}    first_name=${register_merchant_first_name}    last_name=${register_merchant_last_name}    password=${password}
    ...    password_confirm=${password}
    Response Should Be 201 Created    ${response}
    ${user_id_1}=    User Id must be returned in ${response}
    ${response}=    Register merchant    username=${register_merchant_username2}    email=${register_merchant_email}    first_name=${register_merchant_first_name}    last_name=${register_merchant_last_name}    password=${password}
    ...    password_confirm=${password}
    Response Should Be 201 Created    ${response}
    ${user_id_2}=    User Id must be returned in ${response}
    ${response}=    Register merchant    username=${register_merchant_username3}    email=${register_merchant_email}    first_name=${register_merchant_first_name}    last_name=${register_merchant_last_name}    password=${password}
    ...    password_confirm=${password}
    Response Should Be 201 Created    ${response}
    ${user_id_3}=    User Id must be returned in ${response}
    ${response}=    Register merchant    username=${register_merchant_username4}    email=${register_merchant_email}    first_name=${register_merchant_first_name}    last_name=${register_merchant_last_name}    password=${password}
    ...    password_confirm=${password}
    Response Should Be 201 Created    ${response}
    ${user_id_4}=    User Id must be returned in ${response}
    ${query_string}=    Create Dictionary    username=${search_merchant_keyword}
    # Search Merchant with userId=${username}
    ${response}=    Search Merchant    ${query_string}
    Response Should Be 200 OK    ${response}
    Number of Merchant Search Result Should Be    ${response}    3
    ${merchant_json}=    Find Merchant Username In Response Text    ${response}    ${user_id_1}
    Verify Search Merchant Response    ${merchant_json}    ${user_id_1}    ${register_merchant_username}    ${register_merchant_first_name}    ${register_merchant_last_name}    ${register_merchant_email}
    ${merchant_json}=    Find Merchant Username In Response Text    ${response}    ${user_id_2}
    Verify Search Merchant Response    ${merchant_json}    ${user_id_2}    ${register_merchant_username2}    ${register_merchant_first_name}    ${register_merchant_last_name}    ${register_merchant_email}
    ${merchant_json}=    Find Merchant Username In Response Text    ${response}    ${user_id_4}
    Verify Search Merchant Response    ${merchant_json}    ${user_id_4}    ${register_merchant_username4}    ${register_merchant_first_name}    ${register_merchant_last_name}    ${register_merchant_email}

*** Keywords ***
Delete Merchants
    Delete Wemall User From Prefix    ${db}    ${robot_prefix}
    Delete Wemall User From Prefix    ${db}    ${register_merchant_username2}
    Delete Wemall User From Prefix    ${db}    ${register_merchant_username3}
    Delete Wemall User From Prefix    ${db}    ${register_merchant_username4}
